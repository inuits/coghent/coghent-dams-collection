import util

from app import policy_factory
from apps.coghent.resources.base_resource import CoghentBaseResource
from flask import Blueprint, request, after_this_request
from flask_restful import Api
from inuits_policy_based_auth import RequestContext
from validator import box_visit_schema

api_bp = Blueprint("box_visit", __name__)
api = Api(api_bp)


class BoxVisit(CoghentBaseResource):
    @policy_factory.apply_policies(RequestContext(request, ["read-box-visit"]))
    def get(self):
        skip = int(request.args.get("skip", 0))
        limit = int(request.args.get("limit", 20))
        item_type = request.args.get("type", None)
        type_filter = f"type={item_type}&" if item_type else ""
        ids = request.args.get("ids", None)
        if ids:
            ids = ids.split(",")
        box_visits = self.storage.get_box_visits(skip, limit, item_type, ids)
        box_visits["limit"] = limit
        if skip + limit < box_visits["count"]:
            box_visits[
                "next"
            ] = f"/box_visits?{type_filter}skip={skip + limit}&limit={limit}"
        if skip > 0:
            box_visits[
                "previous"
            ] = f"/box_visits?{type_filter}skip={max(0, skip - limit)}&limit={limit}"
        return box_visits

    @policy_factory.apply_policies(RequestContext(request, ["create-box-visit"]))
    def post(self):
        content = request.get_json()
        return self._create_box_visit(content)


class BoxVisitDetail(CoghentBaseResource):
    @policy_factory.apply_policies(RequestContext(request, ["read-box-visit"]))
    def get(self, id):
        box_visit = self._abort_if_item_doesnt_exist("box_visits", id)
        box_visit = self._add_relations_to_metadata(
            box_visit, "box_visits", sort_by="order"
        )
        return box_visit

    @policy_factory.apply_policies(RequestContext(request, ["update-box-visit"]))
    def put(self, id):
        box_visit = self._abort_if_item_doesnt_exist("box_visits", id)
        content = request.get_json()
        self._abort_if_not_valid_json("BoxVisit", content, box_visit_schema)
        box_visit = self.storage.update_item_from_collection(
            "box_visits", util.get_raw_id(box_visit), content
        )
        return box_visit, 201

    @policy_factory.apply_policies(RequestContext(request, ["patch-box-visit"]))
    def patch(self, id):
        box_visit = self._abort_if_item_doesnt_exist("box_visits", id)
        content = request.get_json()
        box_visit = self.storage.patch_item_from_collection(
            "box_visits", util.get_raw_id(box_visit), content
        )
        return box_visit, 201

    @policy_factory.apply_policies(RequestContext(request, ["delete-box-visit"]))
    def delete(self, id):
        box_visit = self._abort_if_item_doesnt_exist("box_visits", id)
        self.storage.delete_item_from_collection(
            "box_visits", util.get_raw_id(box_visit)
        )
        return "", 204


class BoxVisitRelations(CoghentBaseResource):
    @policy_factory.apply_policies(RequestContext(request, ["get-box-visit-relations"]))
    def get(self, id):
        box_visit = self._abort_if_item_doesnt_exist("box_visits", id)

        @after_this_request
        def add_header(response):
            response.headers["Access-Control-Allow-Origin"] = "*"
            return response

        return self.storage.get_collection_item_relations(
            "box_visits", util.get_raw_id(box_visit)
        )

    @policy_factory.apply_policies(RequestContext(request, ["add-box-visit-relations"]))
    def post(self, id):
        box_visit = self._abort_if_item_doesnt_exist("box_visits", id)
        content = request.get_json()
        relations = self.storage.add_relations_to_collection_item(
            "box_visits", util.get_raw_id(box_visit), content, False
        )
        return relations, 201

    @policy_factory.apply_policies(
        RequestContext(request, ["update-box-visit-relations"])
    )
    def put(self, id):
        box_visit = self._abort_if_item_doesnt_exist("box_visits", id)
        content = request.get_json()
        relations = self.storage.update_collection_item_relations(
            "box_visits", util.get_raw_id(box_visit), content, False
        )
        return relations, 201

    @policy_factory.apply_policies(
        RequestContext(request, ["patch-box-visit-relations"])
    )
    def patch(self, id):
        box_visit = self._abort_if_item_doesnt_exist("box_visits", id)
        content = request.get_json()
        relations = self.storage.patch_collection_item_relations(
            "box_visits", util.get_raw_id(box_visit), content, False
        )
        return relations, 201

    @policy_factory.apply_policies(
        RequestContext(request, ["delete-box-visit-relations"])
    )
    def delete(self, id):
        box_visit = self._abort_if_item_doesnt_exist("box_visits", id)
        content = request.get_json()
        self.storage.delete_collection_item_relations(
            "box_visits", util.get_raw_id(box_visit), content, False
        )
        return "", 204


class BoxVisitRelationsAll(CoghentBaseResource):
    @policy_factory.apply_policies(RequestContext(request, ["get-box-visit-relations"]))
    def get(self, id):
        box_visit = self._abort_if_item_doesnt_exist("box_visits", id)

        @after_this_request
        def add_header(response):
            response.headers["Access-Control-Allow-Origin"] = "*"
            return response

        return self.storage.get_collection_item_relations(
            "box_visits", util.get_raw_id(box_visit), include_sub_relations=True
        )


api.add_resource(BoxVisit, "/box_visits")
api.add_resource(BoxVisitDetail, "/box_visits/<string:id>")
api.add_resource(BoxVisitRelations, "/box_visits/<string:id>/relations")
api.add_resource(BoxVisitRelationsAll, "/box_visits/<string:id>/relations/all")
